package com.example.demo.controllers;

import org.openapitools.api.ExpensesApi;
import org.openapitools.model.BodyResponseGetExpenses;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
public class CompanyController implements ExpensesApi {
    @Override
    public ResponseEntity<BodyResponseGetExpenses> listExpenses(@Valid @RequestParam(value = "key_empresa", required = false) String keyEmpresa, @Valid Integer idusuario, @Valid Boolean listarfiliais, @Valid String tipodata, @Valid LocalDate datainicio, @Valid LocalDate datafim, @Valid Integer idstatusaprovacao, @Valid Integer pagamento, @Valid String codigousuario, @Valid String cpf, @Valid Integer idstatusaprovacaorelatorio) {
        return null;
    }
}
